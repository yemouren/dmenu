/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1; /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;  /* -F  option; if 0, dmenu doesn't use fuzzy matching  */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {"FiraCode Nerd Font:size=13"};
static const char *prompt =
    NULL; /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
    /*     fg         bg       */
    [SchemeNorm] = {"#839496", "#002b36"},
    [SchemeSel] = {"#839496", "#073642"},
    [SchemeOut] = {"#eeeeee", "#00ffff"},
    [SchemeSelHighlight] = {"#ffc978", "#005577"},
    [SchemeNormHighlight] = {"#ffc978", "#222222"},

};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines = 0;
static unsigned int columns = 0;
static unsigned int maxhist = 64;
static int histnodup = 1; /* if 0, record repeated histories */

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
